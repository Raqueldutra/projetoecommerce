*** Settings ***
Resource             ../Resource/Resource.robot
Resource             ../Resource/Pages/cadastro_usuario.robot
Test Setup           Nova sessão   
Test Teardown        Encerra sessão
Suite Setup          Initialize Random Variables


*** Test Cases ***

Cenário: CTO1- Cadastrar usuário
    [tags]   cadastro 
    Dado que acessei a pagina de Login
    Quando informo um email valido mo formulario de crie sua conta aqui 
    E clico em Cadastrar
    E preencho os campos obrigatorios do formulario
    E clico em Registrar
    Então sou redirecionado para meu carrinho de compras


