# Projeto Eccomerce

Aplica teste funcional de cadastro e login no site  http://automationpractice.com/, utilizando as ferramentas: selenium Web Drive e Robot Framework.

# Funcionalidades testadas
* Cadastro de usuario
* Login com sucesso
* Login inválido
* Login em branco

# Requisitos
* [Phyton 3.8+](https://www.python.org/downloads/)
* Robot Framework: pip install robotframework
* Chrome 83+
* [Chromedriver 85+](https://github.com/SeleniumHQ/selenium/wiki/ChromeDriver)

# Instalando dependências
* pip install -r requirements.tx

# Executando o projeto
* robot -d ./report Tests\

Serão executados todos os casos de testes e os detalhes com os prints do resultado de cada caso de teste estarão disponíveis em relatório HTML no diretório /report.
