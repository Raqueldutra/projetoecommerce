*** Settings ***
Library         SeleniumLibrary
Library         String

*** Variables ***

${cadastro_url}                            http://automationpractice.com/index.php?controller=authentication&back=my-account    
${cadastro_email}                           id:email_create    
${cadastro_botao_cadastrar}                xpath=.//span[contains(., 'Create an account')]
${cadastro_primeiro_nome}                  id:customer_firstname
${cadastro_ultimo_nome}                    id:customer_lastname
${cadastro_senha}                          id:passwd
${cadastro_endereco}                       id:address1
${cadastro_cidade}                         id:city
${cadastro_pais}                           id:id_state
${cadastro_cep}                            id:postcode
${cadastro_fone}                           id:phone_mobile
${cadastro_botao_registrar}                xpath:.//span[contains(., 'Register')]  

*** Keywords ***

Initialize Random Variables
    ${cadastro_random_name}=   Generate random string  8  [LOWER]
    Set global variable  ${cadastro_random_name}


Dado que acessei a pagina de Login
    Go to                         ${cadastro_url} 

Quando informo um email valido mo formulario de crie sua conta aqui

    Input Text                    ${cadastro_email}              ${cadastro_random_name}@email.com

E clico em Cadastrar                 

    Click Element                 ${cadastro_botao_cadastrar}    

E preencho os campos obrigatorios do formulario

  Wait Until Element Is Visible      ${cadastro_primeiro_nome}  
  Input Text                         ${cadastro_primeiro_nome}     Robot          
  Input Text                         ${cadastro_ultimo_nome}       Framework
  Input Text                         ${cadastro_senha}             123456
  Input Text                         ${cadastro_endereco}          Rua teste
  Input Text                         ${cadastro_cidade}            Florianopolis
  Select From List By Index          ${cadastro_pais}              1     
  Input Text                         ${cadastro_cep}               00000
  Input Text                         ${cadastro_fone}              489998656


E clico em Registrar

    Click Element                    ${cadastro_botao_registrar}

Então sou redirecionado para meu carrinho de compras
  
    Page Should contain               My account
                                  
