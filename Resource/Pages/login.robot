*** Settings ***
Library         SeleniumLibrary

*** Variables ***

${login_url}                                  http://automationpractice.com/index.php?controller=authentication&back=my-account      
${login_email}                                id:email  
${login_senha}                                id:passwd
${login_botao}                                xpath:.//span[contains(., 'Sign in')]

*** Keywords ***


Dado que estou na página de login

    Go to               ${login_url}  

Quando eu preencho os campos de login e senha com dados validos

    Login with        ${cadastro_random_name}@email.com     123456
    Login button

Então sou autenticado com sucesso
    
   Page Should contain           My account

Quando eu não preencho os campos de login e senha

    Login button

Quando eu preencho os campos de login e senha com dados invalidos

    Login with        usuarioincorreto         111111
    Login button

Então vejo a mensagem informando que o usuario é invalido

    Page Should contain           Invalid email address.

Então o endereço de email é requerido

    Page Should contain           An email address required.

Login with
    [Arguments]        ${usuario}             ${senha}

    input text         ${login_email}        ${usuario} 
    input text         ${login_senha}        ${senha}

Login button
    Click Element      ${login_botao}  
       